<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Rabbani Construction</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{URL::to('public/fe/css/style.css')}}" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body onload="myFunction()" style="margin:0;">

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img src="{{URL::to('public/fe/images/tt_logo.png')}}" alt="logo" width="50" height="50" style="padding:5px;" />
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{URL::to('/')}}">HOME</a></li>
                        <li><a href="{{URL::to('about-us')}}">ABOUT US</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJECT <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">RESIDENTIAL</a></li>
                                <li><a href="#">COMMERCIAL</a></li>
                                <li><a href="#">RESIDENTIAL & COMMERCIAL</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">GALLERY<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">ACTIVITY</a></li>
                                <li><a href="#">INTERIOR</a></li>
                                <li><a href="#">EXTERIOR</a></li>
                            </ul>
                        </li>
                        <li><a href="#">CONTACT</a></li>

                    </ul>

                </div>
            </div>
        </nav>



@yield('main_content')

       
        <div id="loader"></div>
        <script type="text/javascript" src="{{URL::to('public/fe/js/script.js')}}"></script>
    </body>
</html>
