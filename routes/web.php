<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/','WelcomeController@index');


Route::get('/fun','AdminController@index');
Route::post('/admin-login-check', 'AdminController@admin_login_check' );
Route::get('/dashboard', 'SuperAdminController@index' );
Route::get('/logout', 'SuperAdminController@logout' );

Route::get('/about-us', 'AboutUsController@index' );
Route::get('/add-about-us', 'AboutUsController@add_about_us' );
Route::post('/save-about-us', 'AboutUsController@save_about_us' );